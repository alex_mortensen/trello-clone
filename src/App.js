import React, { useState } from "react";

export default props => {
  const [cards, setCards] = useState([
    { id: Date.now(), position: 1, value: "This is an item." }
  ]);

  const Card = props => {
    const { card, column } = props;

    const updateCard = updates => {
      const updatedCards = cards.map(crd =>
        card.id === crd.id ? { ...card, ...updates } : crd
      );
      setCards(updatedCards);
    };

    return (
      <div className="card">
        {!column.first && (
          <button onClick={() => updateCard({ position: card.position - 1 })}>
            &lt;
          </button>
        )}
        {card.value}
        {!column.last && (
          <button onClick={() => updateCard({ position: card.position + 1 })}>
            &gt;
          </button>
        )}
      </div>
    );
  };

  const Column = props => {
    const { name, color, index } = props;

    const addCard = () => {
      const value = prompt("Please enter a card description.");
      const card = { id: Date.now(), position: index, value: value };
      setCards(cards.concat(card));
    };

    return (
      <div className={`column ${color}`}>
        <h3>{name}</h3>
        <button onClick={addCard} className="new-button">
          + Add an item
        </button>
        {cards
          .filter(card => card.position === index)
          .map(card => (
            <Card key={card.id} card={card} column={props} />
          ))}
      </div>
    );
  };

  return (
    <div className="wrapper">
      <Column name="backlog" color="red" index={0} first />
      <Column name="to do" color="yellow" index={1} />
      <Column name="in progress" color="green" index={2} />
      <Column name="completed" color="blue" index={3} last />
    </div>
  );
};
